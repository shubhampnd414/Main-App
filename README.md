<h2>Blog App</h2>

Blog App is an web application which allows a user to create  ,view,edit,delete blog after authentication.

**Technology used**

- HTML,CSS3,Javascript,Node js
<h6>Framework used -</h6>
- Express.js , Bootstrap 4
<h6>Database used -</h6> 
- MongoDB Atlas(cloud)
<h6>Html template used</h6>
- EJS

**Directory structure**

- server.js is the main server file.

- blogs/ directory contains routes files.
  - blogs/Routes.js file contains different routes to handle GET/POST request
  - blogs/RouteController.js  file contains all middlewares for handling routes requests

- operation/connectDB.js is used to connect with mongoDB atlas.


- public/
  - public/scripts/ directory contains all scripts files. 
  - public/styles/ directory contains all css files.


- models directory contains schema for collections(myblogs,users) for database(mongoDB)


**Layouts**
- views/ directory contains blogs pages(blogs,edit,detail,delete,create)
- views/partials  directory contains contains common header,errors,footer pages.
- views/user directory contains auth(signup,signin) pages

**Installation**
- Use npm init to initailiaze npm package
- followed by npm install
- change the mongoDB connection string in operation/connectDB.js with the respective password
- use nodemon to start the node server



**Process**

- Intially when user visits the homepage , can view all blogs. After, beign authenticated a user can create blog or like any blog. For authorization the server wil generate a JWT token(consisting the used id,name,email), this hash token will then be stored in the user browser as a cookies . Now, this token will be used to signin user when needed. Ajax is used to create GET/POST request.

**Basic validations-**

- Same email id cannot be used to create new user
- Username,email,password are required
- Blog title,category,content are required


**Features**

- Signup/Signin authentication
- Filter option for blogs categories
- Like blog button (requires authentication)
- Edit blog (requires authentication)
- Delete blog (requires authentication)
- Network status update
- Responsive design
